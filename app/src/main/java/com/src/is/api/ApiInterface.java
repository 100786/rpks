package com.src.is.api;

import com.src.is.models.GenericResponse;
import com.src.is.models.GetSubjectModel;
import com.src.is.models.Login;
import com.src.is.models.RetiredUserList;
import com.src.is.models.SignUpModel;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by insonix on 26/12/16.
 */

public interface ApiInterface {
//    @GET("RetrofitAndroidObjectResponse")
//    Call<Example> getRetroftResponse();


    @GET("getSubject.php")
    Call<GetSubjectModel> getSubject();

    @FormUrlEncoded
    @POST("/userRegister.php")
    Observable<SignUpModel> getRegister(@Field("userName") String username, @Field("email") String email, @Field("phone") String phone, @Field("address") String address, @Field("password") String password, @Field("device_type") String device_type, @Field("device_token") String device_token, @Field("subjectId") int subjectId, @Field("subSubjectId") int subsubjectId, @Field("user_type") int type, @Field("api_type") int api_type);

    @GET("/userLogin.php")
    Observable<Login> login(@Query("loginParam") String userName, @Query("password") String pwd);

    @GET("joingroup.php")
    Observable<GenericResponse> joinGroup(@Query("subsubjectId") String subSubjectId,@Query("user_id") String userId);

    @GET("inviteUser.php")
    Observable<GenericResponse> invite(@Query("email") String email);

    //Send usertype as 1 to get retd listing or 2 to get ks listing

    @GET("getUserBySubSubjectId.php")
    Observable<RetiredUserList> getRetiredUserList(@Query("user_type") String userType,@Query("subSubjectId") String subSubjectId);

    @Multipart
    @POST("/userprofile.php")
    Observable<GenericResponse> updateProfile(@Part("userName") RequestBody name, @Part("email") RequestBody email, @Part("mobileNo") RequestBody mobile, @Part("subjectId") RequestBody subjectId, @Part("subSubjectId") RequestBody subSubjectId, @Part("address") RequestBody address, @Part("password") RequestBody pwd, @Part("device_type") String device_type, @Part("device_token") RequestBody device_token, @Part("hourlyRate") RequestBody hourlyRate, @Part("image") RequestBody image, @Part("experience") RequestBody experience, @Part("lastOrganization") RequestBody lastOrganization,
                                              @Part("designation") RequestBody designation, @Part("description") RequestBody description, @Part("videoProfile") RequestBody videoProfile, @Part("userId"
    ) RequestBody userId);
}
