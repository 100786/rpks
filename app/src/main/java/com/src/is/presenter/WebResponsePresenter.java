package com.src.is.presenter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.Toast;

import com.src.is.models.GetSubjectModel;
import com.src.is.api.ApiClient;
import com.src.is.api.ApiInterface;
import com.src.is.models.GenericResponse;
import com.src.is.utils.RPKSApplication;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by insonix on 26/12/16.
 */

public class WebResponsePresenter {
    Context mContext;
    ApiInterface apiService;
    ArrayList<GetSubjectModel> arrayListGetSubject;
    AlertDialog alert;
    WebResponseView mView;

    public WebResponsePresenter(WebResponseView view) {
        mView=view;
        arrayListGetSubject=new ArrayList<>();
    }

    public void getSubject() {
        mView.onShowProgress();
        apiService = RPKSApplication.getInstance().initApi();
        Call<GetSubjectModel> call = apiService.getSubject();
        call.enqueue(new Callback<GetSubjectModel>() {
            @Override
            public void onResponse(Call<GetSubjectModel> call, Response<GetSubjectModel> response) {
                Log.d("res", String.valueOf(response.body().getStatus()));
                if (response.body().getStatus().equals("true")) {
                    mView.onHideProgress();
                    GetSubjectModel getSubjectModel=new GetSubjectModel();
                    getSubjectModel.setStatus(response.body().getStatus());
                    getSubjectModel.setMessage(response.body().getMessage());
                    getSubjectModel.setSubjectInfo(response.body().getSubjectInfo());
                    mView.setSubjectList(getSubjectModel);
               }
            }

            @Override
            public void onFailure(Call<GetSubjectModel> call, Throwable t) {
                mView.onHideProgress();
                mView.onServerErrorMsg(t.getMessage());
            }
        });

    }



    void getResponse(Call<GenericResponse> call) {


        call.enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                if (response.body().getStatus().equals("true")) {
                    Toast.makeText(mContext, "true", Toast.LENGTH_LONG).show();
                } else {


                }

                Log.d("res", String.valueOf(response.body().getStatus() + "" + response.body().getMessage()));
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                // dialogRetry(call, false,mContext);
            }
        });

    }

    public static boolean isCallSuccess(Response response) {
        int code = response.code();
        return (code >= 200 && code < 400);
    }

    public void dialogRetry(final boolean isConnected, final Context mContext) {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        // Write your code here to execute after dialog closed
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        // Write your code here to execute after dialog closed
                        Toast.makeText(mContext, "cancel", Toast.LENGTH_SHORT).show();
                    }
                });
        alert = builder.create();
        alert.show();


    }


}
