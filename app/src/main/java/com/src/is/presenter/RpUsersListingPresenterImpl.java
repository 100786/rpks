package com.src.is.presenter;

import android.util.Log;

import com.src.is.api.ApiInterface;
import com.src.is.models.GenericResponse;
import com.src.is.models.Login;
import com.src.is.models.RetiredUserList;
import com.src.is.utils.Constants;
import com.src.is.utils.RPKSApplication;
import com.src.is.view.RpUsersListView;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.content.ContentValues.TAG;


/**
 * Created by insonix on 8/3/17.
 */
public class RpUsersListingPresenterImpl implements RpUsersListingPresenter {


    private final RpUsersListView mView;
    private ApiInterface apiInterface;

    public RpUsersListingPresenterImpl(RpUsersListView view) {
        this.mView = view;
    }

    @Override
    public void getListing(String groupId) {

        apiInterface = RPKSApplication.getInstance().initApi();
        apiInterface.getRetiredUserList(String.valueOf(Constants.UserTypeDef.RPUSERTYPE),groupId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<RetiredUserList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: " + e.getMessage());
                    }

                    @Override
                    public void onNext(RetiredUserList retiredUserList) {
                        mView.setRpUsersList(retiredUserList);
                    }
                });
    }

    @Override
    public void joinGroup(String id, String subGroupId) {
        apiInterface = RPKSApplication.getInstance().initApi();
        Log.i(TAG, "joinGroup: "+id);
        apiInterface.joinGroup(subGroupId,id)
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Subscriber<GenericResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "onError: "+e.getMessage() );
            }

            @Override
            public void onNext(GenericResponse response) {
                mView.onSuccesMsg();
            }
        });
    }

}
