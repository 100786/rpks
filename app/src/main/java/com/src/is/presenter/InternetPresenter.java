package com.src.is.presenter;

import android.content.Context;

import com.src.is.view.activity.DialogInternet;

/**
 * Created by insonix on 26/12/16.
 */

public class InternetPresenter  {
    DialogInternet dialogInternet;
    public  InternetPresenter(DialogInternet dialogInternet, Context context){
        this.dialogInternet=dialogInternet;
        dialogInternet.dialogRetry(context);
    }

}
