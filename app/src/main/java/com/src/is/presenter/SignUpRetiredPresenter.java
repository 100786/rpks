package com.src.is.presenter;

import android.content.Context;
import android.util.Log;

import com.src.is.models.GetSubjectModel;
import com.src.is.models.SignUpModel;
import com.src.is.utils.Constants;
import com.src.is.utils.MyPreferences;
import com.src.is.api.ApiClient;
import com.src.is.api.ApiInterface;
import com.src.is.view.SignUpSubmitView;
import com.src.is.view.SignUpView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.src.is.utils.Constants.OS;

/**
 * Created by insonix on 22/12/16.
 */

public class SignUpRetiredPresenter implements SignUpSubmitView{

    private static final String TAG = "SignUpRetiredPresenter";
    SignUpView mView;
    ApiClient apiClient;
    ApiInterface apiService;
    Context mContext;

    public SignUpRetiredPresenter(SignUpView mView, Context context) {
        mContext = context;
        this.mView = mView;
        apiClient = new ApiClient(context);
    }

    public void getSubject() {

        apiService = apiClient.getClient().create(ApiInterface.class);
        Call<GetSubjectModel> call = apiService.getSubject();
        call.enqueue(new Callback<GetSubjectModel>() {
            @Override
            public void onResponse(Call<GetSubjectModel> call, Response<GetSubjectModel> response) {
                if (response.body().getStatus().equals("true")) {
                    GetSubjectModel getSubjectModel = new GetSubjectModel();
                    getSubjectModel.setStatus(response.body().getStatus());
                    getSubjectModel.setMessage(response.body().getMessage());
                    getSubjectModel.setSubjectInfo(response.body().getSubjectInfo());
                    mView.setSubjectList(getSubjectModel);
                }
            }

            @Override
            public void onFailure(Call<GetSubjectModel> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.getMessage() );
            }
        });

    }


    @Override
    public void onSubmit(String userName, String password, String confirmPassword, String email, String address, String contact, int subjectId, int subSubjectId,int userType,int apiType) {

        if (onValidate(userName, email, contact, address, password, confirmPassword)) {
            mView.onHideKeyboard();
            mView.onShowProgress();
            apiService = apiClient.getClient().create(ApiInterface.class);
            Observable<SignUpModel> signedService = apiService.getRegister(userName,email,contact, address, password, OS, MyPreferences.getInstance().getString(MyPreferences.Key.FCM_TOKEN), subjectId, subSubjectId, userType,apiType);
            signedService.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<SignUpModel>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            mView.onHideProgress();
                            Log.i(TAG, "onFailure: "+e.getMessage());
                        }

                        @Override
                        public void onNext(SignUpModel signUpModel) {
                            if(signUpModel.validateStatus()){
                                mView.onHideProgress();
                                mView.onSuccesMsg();
                                mView.nextView(signUpModel);
                            }
                        }
                    });
        }
    }



    @Override
    public void onKsSubmit(String userName, String password, String confirmPassword, String email, String address, String contact, int subjectId, int subSubjectId,int userType,int apiType) {
        if (onValidateks(userName, password, confirmPassword, email, contact)) {
            mView.onShowProgress();
            apiService = apiClient.getClient().create(ApiInterface.class);
            //2 is KS type
            final Observable<SignUpModel> signedService = apiService.getRegister(userName,email, contact, "My Address",password,Constants.OS, MyPreferences.getInstance().getString(MyPreferences.Key.FCM_TOKEN), subjectId,subSubjectId,userType,apiType);
            signedService.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<SignUpModel>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            mView.onHideProgress();
                            Log.i(TAG, "onFailure: "+e.getMessage());
                        }

                        @Override
                        public void onNext(SignUpModel response) {
                            if(response.validateStatus()){
                                mView.onHideProgress();
                                mView.onSuccesMsg();
                                MyPreferences.getInstance().put(MyPreferences.Key.USERID,response.getUserId());
                                mView.nextView(response);
                            }
                        }

                    });
        }
    }


    private boolean onValidate(String userName, String email, String contact, String address, String password, String confirmPassword) {
        if (userName.isEmpty()) {
            mView.showUserNameError();
            return false;
        } else if (email.isEmpty()) {
            mView.showEmailError();
            return false;
        } else if (!email.matches(Constants.emailPattern)) {
            mView.showEmailPatternError();
            return false;
        } else if (contact.isEmpty()) {
            mView.showContactError();
            return false;
        } else if (contact.length() > 15) {
            mView.showContactLengthError();
            return false;
        } else if (address.isEmpty()) {
            mView.showAddressError();
            return false;
        } else if (password.isEmpty()) {
            mView.showPasswordError();
            return false;
        } else if (confirmPassword.isEmpty()) {
            mView.showConfirmPasswordError();
            return false;
        } else if (!password.matches(confirmPassword)) {
            mView.showMatchesPasswordError();
            return false;
        } else if (password.length() < 4) {
            mView.showPasswordLengthError();
            return false;
        }
        return true;
    }

    private boolean onValidateks(String userName, String password, String confirmPassword, String email, String contact) {
        if (userName.isEmpty()) {
            mView.showUserNameError();
            return false;
        } else if (email.isEmpty()) {
            mView.showEmailError();
            return false;
        } else if (!email.matches(Constants.emailPattern)) {
            mView.showEmailPatternError();
            return false;
        } else if (contact.isEmpty()) {
            mView.showContactError();
            return false;
        } else if (contact.length() > 15) {
            mView.showContactLengthError();
            return false;
        } else if (password.isEmpty()) {
            mView.showPasswordError();
            return false;
        } else if (confirmPassword.isEmpty()) {
            mView.showConfirmPasswordError();
            return false;
        } else if (!password.matches(confirmPassword)) {
            mView.showMatchesPasswordError();
            return false;
        } else if (password.length() < 4) {
            mView.showPasswordLengthError();
            return false;
        }
        return true;
    }
}
