package com.src.is.presenter;

import com.src.is.models.GetSubjectModel;
import com.src.is.models.SignUpModel;
import com.src.is.view.BaseView;

import java.util.ArrayList;

/**
 * Created by insonix on 27/12/16.
 */

public interface WebResponseView extends BaseView{


    void setSubjectList(GetSubjectModel getSubjectModel);

}
