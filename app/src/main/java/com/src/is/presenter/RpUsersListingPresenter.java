package com.src.is.presenter;

/**
 * Created by insonix on 8/3/17.
 */

public interface RpUsersListingPresenter {

    void getListing(String groupId);
    void joinGroup(String id, String subGroupId);
}
