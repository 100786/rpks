package com.src.is.presenter;

import android.util.Log;

import com.src.is.api.ApiInterface;
import com.src.is.models.GetSubjectModel;
import com.src.is.utils.RPKSApplication;
import com.src.is.view.ProfileRetiredDoneView;
import com.src.is.view.ProfileRetiredView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 21/12/16.
 */


public class ProfileRetiredPresenter implements ProfileRetiredDoneView {

    ProfileRetiredView mView;
    private ApiInterface apiService;

    public ProfileRetiredPresenter(ProfileRetiredView profileRetiredValidations) {
        this.mView = profileRetiredValidations;
    }
    @Override
    public void onDone(String hourlyRate, String experience, String lastWorkExp, String designation) {
        if (onValidate(hourlyRate, experience, lastWorkExp, designation)) {
            mView.onUpdateCompletion();
        }
    }

    @Override
    public void getSubjects() {
        apiService= RPKSApplication.getInstance().initApi();
        Call<GetSubjectModel> call = apiService.getSubject();
        call.enqueue(new Callback<GetSubjectModel>() {
            @Override
            public void onResponse(Call<GetSubjectModel> call, Response<GetSubjectModel> response) {
                if (response.body().getStatus().equals("true")) {
                    GetSubjectModel getSubjectModel = new GetSubjectModel();
                    getSubjectModel.setStatus(response.body().getStatus());
                    getSubjectModel.setMessage(response.body().getMessage());
                    getSubjectModel.setSubjectInfo(response.body().getSubjectInfo());
                    mView.setSubjectList(getSubjectModel);
                }
            }

            @Override
            public void onFailure(Call<GetSubjectModel> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.getMessage() );
            }
        });
    }

    private boolean onValidate(String hourlyRate, String experience, String lastWorkExp, String designation) {
        if (hourlyRate.isEmpty()) {
            mView.showHourlyError();
            return false;
        } else if (experience.isEmpty()) {
            mView.showExperienceError();
            return false;
        } else if (lastWorkExp.isEmpty()) {
            mView.showLastWorkExpError();
            return false;
        } else if (designation.isEmpty()) {
            mView.showDesignationError();
            return false;
        }
        return true;
    }
}
