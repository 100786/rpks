package com.src.is.presenter;

import android.util.Log;

import com.src.is.R;
import com.src.is.api.ApiInterface;
import com.src.is.models.GenericResponse;
import com.src.is.models.Login;
import com.src.is.utils.RPKSApplication;
import com.src.is.view.LoginView;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 6/3/17.
 */
public class LoginPresenterImpl implements LoginPresenter {

    LoginView mView;
    private ApiInterface apiService;

    public LoginPresenterImpl(LoginView mView) {
        this.mView = mView;
    }

    @Override
    public void onLogin(String userName,String pwd) {
        if(mView.validateInput(userName,pwd)){
            mView.onShowProgress();
            apiService=RPKSApplication.getInstance().initApi();
            final Observable<Login> loginService = apiService.login(userName,pwd);
            loginService.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<Login>() {
                        @Override
                        public void onCompleted() {
                            Log.i(TAG, "onCompleted: Operation completed");
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.i(TAG, "onError: "+e.getMessage());
                        }

                        @Override
                        public void onNext(Login loginResponse) {
                            if(loginResponse.validateStatus()){
                                mView.onHideProgress();
                                mView.onNextView(loginResponse);
                            }else{
                                mView.onHideProgress();
                                mView.onErrorMsg(R.string.invalidCred);
                            }
                        }
                    });
        }


    }

    @Override
    public void allocateGroup(String id, String subSubjectId) {
        mView.onShowProgress();
        apiService = RPKSApplication.getInstance().initApi();
        Log.i(TAG, "joinGroup: "+id);
        apiService.joinGroup(subSubjectId,id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GenericResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onHideProgress();
                        Log.e(TAG, "onError: "+e.getMessage() );
                    }

                    @Override
                    public void onNext(GenericResponse response) {
                        mView.onHideProgress();
                        mView.onSuccesMsg();
                    }
                });
    }

}
