package com.src.is.presenter;

/**
 * Created by insonix on 6/3/17.
 */

public interface LoginPresenter {

    void onLogin(String userName,String pwd);

    void allocateGroup(String id, String subSubjectId);
}
