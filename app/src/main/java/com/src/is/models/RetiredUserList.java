
package com.src.is.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class RetiredUserList {

    @SerializedName("message")
    private String mMessage;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("userInfo")
    private List<UserInfo> mUserInfo;

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public List<UserInfo> getUserInfo() {
        return mUserInfo;
    }

    public void setUserInfo(List<UserInfo> userInfo) {
        mUserInfo = userInfo;
    }

}
