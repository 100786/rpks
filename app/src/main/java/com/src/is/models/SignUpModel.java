package com.src.is.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import rx.Observable;

/**
 * Created by insonix on 26/12/16.
 */

public class SignUpModel extends Observable{

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("user id")
    @Expose
    private String userId;

    protected SignUpModel(OnSubscribe f) {
        super(f);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean validateStatus(){
        if(status.equalsIgnoreCase("true"))
            return true;
        return false;
    }

}
