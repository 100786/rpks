package com.src.is.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by insonix on 26/12/16.
 */

public class GenericResponse {

    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("ErrMsg")
    @Expose
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean validateStatus(){
        if(status.equalsIgnoreCase("true"))
            return true;
        return false;
    }
}
