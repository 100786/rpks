package com.src.is.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by insonix on 26/12/16.
 */

public class SubjectInfo implements Parcelable {

    @SerializedName("subjectId")
    @Expose
    private String subjectId;
    @SerializedName("subjectName")
    @Expose
    private String subjectName;
    @SerializedName("subSubject")
    @Expose
    private List<SubjectInfo> subSubject = null;
    @SerializedName("image")
    @Expose
    private String image;

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public List<SubjectInfo> getSubSubject() {
        return subSubject;
    }

    public void setSubSubject(List<SubjectInfo> subSubject) {
        this.subSubject = subSubject;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.subjectId);
        dest.writeString(this.subjectName);
        dest.writeList(this.subSubject);
        dest.writeString(this.image);
    }

    public SubjectInfo() {
    }

    protected SubjectInfo(Parcel in) {
        this.subjectId = in.readString();
        this.subjectName = in.readString();
        this.subSubject = new ArrayList<SubjectInfo>();
        in.readList(this.subSubject, SubjectInfo.class.getClassLoader());
        this.image = in.readString();
    }

    public static final Parcelable.Creator<SubjectInfo> CREATOR = new Parcelable.Creator<SubjectInfo>() {
        @Override
        public SubjectInfo createFromParcel(Parcel source) {
            return new SubjectInfo(source);
        }

        @Override
        public SubjectInfo[] newArray(int size) {
            return new SubjectInfo[size];
        }
    };
}
