package com.src.is.models;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetSubjectModel implements Parcelable {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("subjectInfo")
    @Expose
    private List<SubjectInfo> subjectInfo;

  /*  public GetSubjectModel(String status, String message, List<SubjectInfo> subjectInfo) {
        this.status = status;
        this.message = message;
        this.subjectInfo = subjectInfo;
    }
*/

    protected GetSubjectModel(Parcel in) {
        status = in.readString();
        message = in.readString();
        in.readList(subjectInfo, List.class.getClassLoader());
    }

    public static final Creator<GetSubjectModel> CREATOR = new Creator<GetSubjectModel>() {
        @Override
        public GetSubjectModel createFromParcel(Parcel in) {
            return new GetSubjectModel(in);
        }

        @Override
        public GetSubjectModel[] newArray(int size) {
            return new GetSubjectModel[size];
        }
    };

    public GetSubjectModel() {

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SubjectInfo> getSubjectInfo() {
        return subjectInfo;
    }

    public void setSubjectInfo(List<SubjectInfo> subjectInfo) {
        this.subjectInfo = subjectInfo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(status);
        parcel.writeString(message);
        parcel.writeList(subjectInfo);
    }
}


