
package com.src.is.models;

import com.google.gson.annotations.SerializedName;

public class UserInfo {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("api_type")
    private String mApiType;
    @SerializedName("avlblDates")
    private String mAvlblDates;
    @SerializedName("created_date")
    private String mCreatedDate;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("designation")
    private String mDesignation;
    @SerializedName("device_token")
    private String mDeviceToken;
    @SerializedName("device_type")
    private String mDeviceType;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("email_status")
    private String mEmailStatus;
    @SerializedName("experience")
    private String mExperience;
    @SerializedName("hourlyRate")
    private String mHourlyRate;
    @SerializedName("id")
    private String mId;
    @SerializedName("Image")
    private String mImage;
    @SerializedName("lastWorked")
    private String mLastWorked;
    @SerializedName("modified_date")
    private String mModifiedDate;
    @SerializedName("password")
    private String mPassword;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("subSubjectId")
    private String mSubSubjectId;
    @SerializedName("subject_id")
    private String mSubjectId;
    @SerializedName("token")
    private String mToken;
    @SerializedName("user_name")
    private String mUserName;
    @SerializedName("user_type")
    private String mUserType;
    @SerializedName("videoProfile")
    private String mVideoProfile;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getApiType() {
        return mApiType;
    }

    public void setApiType(String api_type) {
        mApiType = api_type;
    }

    public String getAvlblDates() {
        return mAvlblDates;
    }

    public void setAvlblDates(String avlblDates) {
        mAvlblDates = avlblDates;
    }

    public String getCreatedDate() {
        return mCreatedDate;
    }

    public void setCreatedDate(String created_date) {
        mCreatedDate = created_date;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getDesignation() {
        return mDesignation;
    }

    public void setDesignation(String designation) {
        mDesignation = designation;
    }

    public String getDeviceToken() {
        return mDeviceToken;
    }

    public void setDeviceToken(String device_token) {
        mDeviceToken = device_token;
    }

    public String getDeviceType() {
        return mDeviceType;
    }

    public void setDeviceType(String device_type) {
        mDeviceType = device_type;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getEmailStatus() {
        return mEmailStatus;
    }

    public void setEmailStatus(String email_status) {
        mEmailStatus = email_status;
    }

    public String getExperience() {
        return mExperience;
    }

    public void setExperience(String experience) {
        mExperience = experience;
    }

    public String getHourlyRate() {
        return mHourlyRate;
    }

    public void setHourlyRate(String hourlyRate) {
        mHourlyRate = hourlyRate;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getLastWorked() {
        return mLastWorked;
    }

    public void setLastWorked(String lastWorked) {
        mLastWorked = lastWorked;
    }

    public String getModifiedDate() {
        return mModifiedDate;
    }

    public void setModifiedDate(String modified_date) {
        mModifiedDate = modified_date;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getSubSubjectId() {
        return mSubSubjectId;
    }

    public void setSubSubjectId(String subSubjectId) {
        mSubSubjectId = subSubjectId;
    }

    public String getSubjectId() {
        return mSubjectId;
    }

    public void setSubjectId(String subject_id) {
        mSubjectId = subject_id;
    }

    public String getToken() {
        return mToken;
    }

    public void setToken(String token) {
        mToken = token;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String user_name) {
        mUserName = user_name;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String user_type) {
        mUserType = user_type;
    }

    public String getVideoProfile() {
        return mVideoProfile;
    }

    public void setVideoProfile(String videoProfile) {
        mVideoProfile = videoProfile;
    }

}
