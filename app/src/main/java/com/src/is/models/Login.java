
package com.src.is.models;

public class Login {


    /**
     * status : true
     * userId : 25
     * user_type : 1
     * message : User loggedIn successfully
     */

    private String status;
    private String userId;
    private String user_type;
    private String message;

    public String getSubSubjectId() {
        return subSubjectId;
    }

    public void setSubSubjectId(String subSubjectId) {
        this.subSubjectId = subSubjectId;
    }

    private String subSubjectId;

    public boolean validateStatus() {
        if (status.equalsIgnoreCase("true"))
            return true;
        return false;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
