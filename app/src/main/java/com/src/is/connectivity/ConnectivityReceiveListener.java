package com.src.is.connectivity;

/**
 * Created by insonix on 26/12/16.
 */

public interface ConnectivityReceiveListener {
    void onNetworkConnectionChanged(boolean isConnected);
}
