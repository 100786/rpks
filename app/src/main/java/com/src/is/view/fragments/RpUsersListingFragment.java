package com.src.is.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.src.is.R;
import com.src.is.EventBus.Events;
import com.src.is.EventBus.GlobalBus;
import com.src.is.models.RetiredUserList;
import com.src.is.models.UserInfo;
import com.src.is.presenter.RpUsersListingPresenter;
import com.src.is.presenter.RpUsersListingPresenterImpl;
import com.src.is.utils.ItemClickListener;
import com.src.is.utils.MyPreferences;
import com.src.is.view.RpUsersListView;
import com.src.is.view.adapters.GroupChildMembersAdapter;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 20/12/16.
 */

public class RpUsersListingFragment extends BaseFragment implements RpUsersListView, ItemClickListener {

    @BindView(R.id.recycler_childList)
    RecyclerView recyclerChildList;
    @BindView(R.id.not_avlbl)
    TextView notAvlbl;
    private LinearLayoutManager mLayoutManager;
    GroupChildMembersAdapter groupChildMembersAdapter;
    Events.SubjectEvent subjectEvent;
    RpUsersListingPresenter mPresenter;
    private ArrayList<UserInfo> usersList;
    TextView join;
    private String mId;

    @Subscribe
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_group_child_member, container, false);
        ButterKnife.bind(this, view);
        Toolbar actionBar = (Toolbar) getActivity().findViewById(R.id.appbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(actionBar);
        join = (TextView) actionBar.findViewById(R.id.join_text);
        join.setVisibility(View.VISIBLE);
        subjectEvent = GlobalBus.getBus().getStickyEvent(Events.SubjectEvent.class);
        Log.i(TAG, "onCreateView:RpUsersList " + subjectEvent.getId());
        usersList = new ArrayList<>();
        initRecycler();
        mPresenter = new RpUsersListingPresenterImpl(this);
        mPresenter.getListing(subjectEvent.getId());
        join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.joinGroup(subjectEvent.getId(), MyPreferences.getInstance().getString(MyPreferences.Key.USERID));
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.getListing(subjectEvent.getId());

    }

    private void initRecycler() {
        groupChildMembersAdapter = new GroupChildMembersAdapter(usersList, this);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerChildList.setLayoutManager(mLayoutManager);
        recyclerChildList.setItemAnimator(new DefaultItemAnimator());
        recyclerChildList.setAdapter(groupChildMembersAdapter);
    }

    @Override
    public void setRpUsersList(RetiredUserList listing) {
        if (listing.getUserInfo().size() > 0) {
            usersList.clear();
            usersList.addAll(listing.getUserInfo());
            groupChildMembersAdapter.notifyDataSetChanged();
        } else {
            recyclerChildList.setVisibility(View.GONE);
            notAvlbl.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onParentClick(int position) {
        GlobalBus.getBus().postSticky(new Events.UserEvent(usersList.get(position)));
        ProfileRetiredFragment nextFrag = new ProfileRetiredFragment();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_fragment, nextFrag)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i(TAG, "onDestroyView: ");
        join.setVisibility(View.GONE);
    }

    @Override
    public void onSuccesMsg() {
        super.onSuccesMsg();
        Toast.makeText(getActivity(),"Joined successfully",Toast.LENGTH_LONG).show();
    }
}
