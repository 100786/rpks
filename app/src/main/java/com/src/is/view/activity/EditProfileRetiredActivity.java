package com.src.is.view.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.squareup.picasso.Picasso;
import com.src.is.R;
import com.src.is.models.GetSubjectModel;
import com.src.is.models.SubjectInfo;
import com.src.is.presenter.ProfileRetiredPresenter;
import com.src.is.utils.Camera.CameraUtils;
import com.src.is.utils.CommonUtils;
import com.src.is.utils.MyPreferences;
import com.src.is.utils.RoundedImageView;
import com.src.is.view.ProfileRetiredView;
import com.src.is.view.adapters.NothingSelectedSpinnerAdapter;
import com.src.is.view.adapters.SubjectAdapter;

import org.greenrobot.eventbus.Subscribe;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.ContentValues.TAG;
import static com.src.is.utils.Camera.CameraUtils.REQUEST_CAMERA;
import static com.src.is.utils.Camera.CameraUtils.SELECT_FILE;
import static com.src.is.utils.CommonUtils.getRealPathFromURI;
import static com.src.is.utils.CommonUtils.openVideoIntent;
import static com.src.is.utils.Constants.ACTION_TAKE_VIDEO;

public class EditProfileRetiredActivity extends BaseActivity implements ProfileRetiredView {

    @BindView(R.id.user_profile_photo)
    RoundedImageView userProfilePhoto;
    @BindView(R.id.input_hourlyrate)
    EditText inputHourlyrate;
    @BindView(R.id.spinner_currency)
    Spinner spinnerCurrency;
    @BindView(R.id.input_description)
    EditText inputDescription;
    @BindView(R.id.input_experience)
    EditText inputExperience;
    @BindView(R.id.input_last_work_company)
    EditText inputLastWorkCompany;
    @BindView(R.id.input_designation)
    EditText inputDesignation;
    @BindView(R.id.text_upload_video)
    TextView textUploadVideo;

    SubjectAdapter subjectAdapter;

    /*@BindView(R.id.input_calendar)
    EditText inputCalendar;*/
    Calendar calendar;
    ArrayAdapter currencyAdapter;
    DatePickerDialog.OnDateSetListener date;
    SimpleDateFormat df;
    //    @BindView(R.id.image_backbtn)
//    ImageView imageBackbtn;

    @BindView(R.id.card_view)
    CardView cardView;

    @BindView(R.id.card_view1)
    CardView cardView1;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    String selectedImagePath;

    @BindView(R.id.image_video_icon)
    ImageView imageVideoIcon;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.edit_bttn)
    ImageView editBttn;
    @Nullable
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.image_hover)
    ImageView imageHover;
    @BindView(R.id.image_video_thumbnail)
    ImageView imageVideoThumbnail;
    @BindView(R.id.videoView)
    RelativeLayout videoView;
    @BindView(R.id.input_username)
    EditText inputUsername;
    @BindView(R.id.input_email)
    EditText inputEmail;
    @BindView(R.id.input_mpbile)
    EditText inputMpbile;
    @BindView(R.id.spinner_areaExpertise)
    Spinner spinnerAreaExpertise;
    @BindView(R.id.linear_area)
    CardView linearArea;
    @BindView(R.id.spinner_category)
    Spinner spinnerCategory;
    @BindView(R.id.category_lay)
    CardView categoryLay;
    @BindView(R.id.input_address)
    EditText inputAddress;
    @BindView(R.id.input_password)
    EditText inputPassword;
    @BindView(R.id.focus_thief)
    View focusThief;
    private Bitmap bmThumbnail;
    ProfileRetiredPresenter profileRetiredPresenter;
    String mHourlyRate, mExperience, mDesignation, mLastWorkExp,mUseName,mPassword;
    Uri selectedImageUri;
    private int mSubjectId,mSubSubjectId;

    public static Intent createIntent(Context ctx) {
        Intent intent = new Intent(ctx, EditProfileRetiredActivity.class);
        return intent;
    }

    @Subscribe
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile_retired);
        ButterKnife.bind(this);
        profileRetiredPresenter = new ProfileRetiredPresenter(this);
//        toolbar.setTitle("Edit Profile");           //TODO later
        editBttn.setVisibility(View.VISIBLE);
        currencyAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.currency));
        spinnerCurrency.setAdapter(currencyAdapter);
        spinnerCurrency.setSelection(0);
        userProfilePhoto.setImageResource(R.drawable.bg);
        inputUsername.clearFocus();
        profileRetiredPresenter.getSubjects();
//        calendar=Calendar.getInstance();
//        inputCalendar.setText(df.format(calendar.getTime()));

    }


    @OnClick({R.id.edit_bttn, R.id.image_video_icon, R.id.text_upload_video, R.id.user_profile_photo})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.edit_bttn:
                mUseName=inputUsername.getText().toString().trim();
                mHourlyRate = inputHourlyrate.getText().toString().trim();
                mExperience = inputExperience.getText().toString().trim();
                mLastWorkExp = inputLastWorkCompany.getText().toString().trim();
                mDesignation = inputDesignation.getText().toString().trim();

                /*userName,email,mobileNo,subjectId,subSubjectId,address,password,device_type,device_token,
                hourlyRate,image,experience,lastOrganization,
designation,description
,videoProfile,userId*/
//                profileRetiredPresenter.onDone(mUseName,mPasswordmHourlyRate, mExperience, mLastWorkExp, mDesignation);
                break;
            case R.id.image_video_icon:
                final Dialog dialog = new Dialog(EditProfileRetiredActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_video_perview);
                VideoView videoPreview = (VideoView) dialog.findViewById(R.id.videoPreview);
                Button btnOk = (Button) dialog.findViewById(R.id.btn_ok);
                videoPreview.setVisibility(View.VISIBLE);
                videoPreview.setVideoURI(selectedImageUri);
                videoPreview.start();
                dialog.show();
                btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                break;
            case R.id.text_upload_video:
                openVideoIntent(EditProfileRetiredActivity.this);
                break;
            case R.id.user_profile_photo:
                CameraUtils.selectImage(EditProfileRetiredActivity.this);
                break;
        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bm;
        switch (requestCode) {
            case ACTION_TAKE_VIDEO:
                VideoTaken(data, resultCode);
                break;
            case SELECT_FILE:
                bm = CameraUtils.getBitmapGallery(data);
                userProfilePhoto.setImageBitmap(bm);
                imageHover.setImageBitmap(bm);
                break;
            case REQUEST_CAMERA:
                bm = CameraUtils.onCaptureImageResult(data);
                userProfilePhoto.setImageBitmap(bm);
                imageHover.setImageBitmap(bm);
                break;
        }
    }

    //To set video thumbnail in video preview after video completion
    private void VideoTaken(Intent data, int resultCode) {
        if (resultCode == RESULT_OK && data != null) {
            imageVideoIcon.setVisibility(View.VISIBLE);
            selectedImageUri = data.getData();
            selectedImagePath = getRealPathFromURI(EditProfileRetiredActivity.this, selectedImageUri);
            Picasso.with(EditProfileRetiredActivity.this).load(selectedImageUri).error(R.mipmap.ic_launcher).into(imageVideoThumbnail);
            Log.i(TAG, "onActivityResult: " + selectedImagePath);
            textUploadVideo.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            bmThumbnail = ThumbnailUtils.createVideoThumbnail(selectedImagePath, MediaStore.Images.Thumbnails.MICRO_KIND);
            imageVideoThumbnail.setImageBitmap(bmThumbnail);
        } else if (resultCode == RESULT_CANCELED) {
            Toast.makeText(this, "Video recording cancelled.",
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Failed to record video",
                    Toast.LENGTH_LONG).show();
        }

    }


    @Override
    public void showHourlyError() {
        Snackbar.make(inputHourlyrate, R.string.hourly_rate_empty_error_msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showExperienceError() {
        Snackbar.make(inputExperience, R.string.experience_empty_error_msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showLastWorkExpError() {
        Snackbar.make(inputLastWorkCompany, R.string.last_workexp_empty_error_msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showDesignationError() {
        Snackbar.make(inputDesignation, R.string.designation_empty_error_msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onUpdateCompletion() {
        MyPreferences.getInstance().put(MyPreferences.Key.FIRST_TIME, true);
        startActivity(new Intent(EditProfileRetiredActivity.this, HomeActivity.class));
    }

    @Override
    public void setSubjectList(GetSubjectModel getSubjectModel) {
        final List<SubjectInfo> subjectInfoList = getSubjectModel.getSubjectInfo();
        subjectAdapter = new SubjectAdapter(EditProfileRetiredActivity.this, android.R.layout.simple_list_item_1, subjectInfoList);
        subjectAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAreaExpertise.setAdapter(new NothingSelectedSpinnerAdapter(subjectAdapter,
                R.layout.contact_spinner_row_nothing_selected,
                this));
        spinnerAreaExpertise.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.i(TAG, "onItemSelected: " + position);
                if (position > 0) {
                    categoryLay.setVisibility(View.VISIBLE);
                    setSubSubjectList(position, subjectInfoList);
                    mSubjectId=Integer.parseInt(subjectInfoList.get(position-1).getSubjectId());        //nothing selected adapter added "hint" as )th item
                    Log.i(TAG, "onItemSelected: "+mSubjectId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                categoryLay.setVisibility(View.GONE);

            }
        });
    }


    private void setSubSubjectList(int position, List<SubjectInfo> parentData) {
        Log.i(TAG, "setSubSubjectList: "+position);
        if (position > 0 && parentData.get(position-1).getSubSubject()!=null) {
            final List<SubjectInfo> subSubjectList=parentData.get(position-1).getSubSubject();
            spinnerCategory.setAdapter(new SubjectAdapter(EditProfileRetiredActivity.this, android.R.layout.simple_list_item_1, subSubjectList));
            spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mSubSubjectId= Integer.parseInt(subSubjectList.get(position).getSubjectId());
                    Log.i(TAG, "onItemSelected: "+mSubSubjectId);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CommonUtils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (CameraUtils.userChoosenTask.equals("Take Photo"))
                        CameraUtils.cameraIntent(EditProfileRetiredActivity.this);
                    else if (CameraUtils.userChoosenTask.equals("Choose from Library"))
                        CameraUtils.galleryIntent(EditProfileRetiredActivity.this);
                } else {
//code for deny
                    Toast.makeText(EditProfileRetiredActivity.this, "Permissions denied", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }

}