package com.src.is.view.activity;

import android.os.Bundle;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.src.is.BuildConfig;
import com.src.is.R;
import com.src.is.models.Login;
import com.src.is.presenter.LoginPresenter;
import com.src.is.presenter.LoginPresenterImpl;
import com.src.is.utils.Constants;
import com.src.is.utils.MyPreferences;
import com.src.is.view.BaseView;
import com.src.is.view.LoginView;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by insonix on 23/12/16.
 */

public class LoginActivity extends BaseActivity implements LoginView {

    BaseView mView;
    LoginPresenter mPresenter;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.ed_username)
    EditText edUsername;
    @BindView(R.id.ed_pwd)
    EditText edPwd;
    private String mUserName, mPassword;
    boolean mPwdHide = true;

    @Subscribe
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mPresenter = new LoginPresenterImpl(this);
        edPwd.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (edPwd.getRight() - edPwd.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (mPwdHide && !edPwd.getText().toString().isEmpty()) {
                            mPwdHide = false;
                            edPwd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        } else {
                            mPwdHide = true;
                            edPwd.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
                        }
                        return true;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onNextView(Login loginResponse) {
        finish();
        MyPreferences.getInstance(LoginActivity.this).put(MyPreferences.Key.USERID,loginResponse.getUserId());
        MyPreferences.getInstance(LoginActivity.this).put(MyPreferences.Key.USER_TYPE,loginResponse.getUser_type());
        if(BuildConfig.APPFLAVOR==Constants.UserTypeDef.RPUSERTYPE){
            mPresenter.allocateGroup(MyPreferences.getInstance().getString(MyPreferences.Key.USERID),loginResponse.getSubSubjectId());
            startActivity(HomeActivity.createIntent(LoginActivity.this));
        }else{
            startActivity(SelectGroupActivity.createIntent(this));
        }
    }

    @Override
    public boolean validateInput(String userName, String pwd) {
        if (userName.isEmpty()) {
            mView.onErrorMsg(R.string.error_empty_usernameemail);
            return false;
        } else if (pwd.isEmpty()) {
            mView.onErrorMsg(R.string.error_empty_password);
            return false;
        }
        return true;
    }

    @OnClick(R.id.btn_login)
    public void onClick() {
        readInputs();
        mPresenter.onLogin(mUserName, mPassword);
    }

    private void readInputs() {
        mUserName = edUsername.getText().toString().trim();
        mPassword = edPwd.getText().toString().trim();
    }

}
