package com.src.is.view;

/**
 * Created by insonix on 27/12/16.
 */

public interface BaseView {

    void onShowProgress();

    void onHideProgress();

    void onHideKeyboard();

    void onErrorMsg(int message);

    void  onSuccesMsg();

    void onServerErrorMsg(String message);
}
