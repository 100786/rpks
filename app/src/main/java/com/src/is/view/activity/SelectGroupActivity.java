package com.src.is.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.src.is.R;
import com.src.is.view.fragments.SelectGroupFragment;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by insonix on 19/12/16.
 */

public class SelectGroupActivity extends BaseActivity implements BackButtonListener {

    BackButtonListener backButtonListener;
    @BindView(R.id.content_fragment)
    FrameLayout contentFragment;
    @BindView(R.id.slidingpane_layout)
    SlidingPaneLayout slidingpaneLayout;
    @BindView(R.id.appbar)
    Toolbar myToolbar;
    @BindView(R.id.menu_icon)
    ImageView menuIcon;
    @BindView(R.id.title)
    TextView title;

    public static Intent createIntent(Context ctx) {
        Intent intent = new Intent(ctx, SelectGroupActivity.class);
        return intent;
    }

    @Subscribe
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_group);
        ButterKnife.bind(this);
        backButtonListener = this;
        myToolbar.setTitle("Home");
        SelectGroupFragment nextFrag = new SelectGroupFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.content_fragment, nextFrag)
                .addToBackStack(null)
                .commit();
        showNavigAnimation();
    }


    private void showNavigAnimation() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                slidingpaneLayout.openPane();
            }
        }, 1500);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                slidingpaneLayout.closePane();
            }
        }, 3000);
    }

    SlidingPaneLayout.PanelSlideListener panelListener = new SlidingPaneLayout.PanelSlideListener() {

        @Override
        public void onPanelClosed(View arg0) {
            Log.d("close", "close");
        }

        @Override
        public void onPanelOpened(View arg0) {
            Log.d("oprn", "oprn");
            SelectGroupFragment nextFrag = new SelectGroupFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_fragment, nextFrag)
                    .addToBackStack(null)
                    .commit();
        }

        @Override
        public void onPanelSlide(View arg0, float arg1) {
            Log.d("slide", "slide");
        }

    };

    @Override
    public void onBackPressed() {
        backButtonListener.onClickBackListener();
        int fragments = getSupportFragmentManager().getBackStackEntryCount();
        if (fragments == 1) {
            finish();
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                getSupportFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int menuItemId = item.getItemId();
        if (menuItemId == R.id.menu_icon) {

        }
        return true;
    }

    @Override
    public void onClickBackListener() {

    }


    @Override
    public void onServerErrorMsg(String message) {

    }

}
