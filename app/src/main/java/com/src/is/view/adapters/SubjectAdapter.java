package com.src.is.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.src.is.R;
import com.src.is.models.SubjectInfo;
import com.src.is.view.activity.SignUpKnowSeekerActivity;

import java.util.HashMap;
import java.util.List;

/**
 * Created by insonix on 3/3/17.
 */


public class SubjectAdapter extends ArrayAdapter<CharSequence> {


    List<SubjectInfo> mCategory;

    public SubjectAdapter(Context context, int resource, List<SubjectInfo> mCategory) {
        super(context, resource);
        this.mCategory = mCategory;
    }


    @Override
    public int getCount() {
        return mCategory.size();
    }

    @Override
    public CharSequence getItem(int i) {
        return mCategory.get(i).getSubjectName();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        view = layoutInflater.inflate(R.layout.adapter_spinner, null);
        TextView textView = (TextView) view.findViewById(R.id.text_spinner);
        textView.setText(mCategory.get(i).getSubjectName());
        return view;
    }
}
