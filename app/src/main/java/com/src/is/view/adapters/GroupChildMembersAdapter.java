package com.src.is.view.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.src.is.R;

import com.src.is.models.UserInfo;
import com.src.is.utils.ItemClickListener;

/**
 * Created by insonix on 20/12/16.
 */

public class GroupChildMembersAdapter extends RecyclerView.Adapter<GroupChildMembersAdapter.ViewHolder> {

    ArrayList<UserInfo> mArraylistMembers;
    Context mContext;
    ItemClickListener mListener;

    public GroupChildMembersAdapter(ArrayList<UserInfo> membersName,ItemClickListener listener) {
        mArraylistMembers = membersName;
        this.mListener=listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.adapter_group_child_members, null);
        mContext = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.textName.setText(mArraylistMembers.get(position).getUserName());
        holder.relativeSelectrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            mListener.onParentClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArraylistMembers.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_person)
        ImageView imagePerson;
        @BindView(R.id.text_name)
        TextView textName;
        @BindView(R.id.text_education)
        TextView textEducation;
        @BindView(R.id.text_exp)
        TextView textExp;
        @BindView(R.id.ratingBar)
        RatingBar ratingBar;
        @BindView(R.id.relative_selectrow)
        RelativeLayout relativeSelectrow;
        @BindView(R.id.card_view)
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
