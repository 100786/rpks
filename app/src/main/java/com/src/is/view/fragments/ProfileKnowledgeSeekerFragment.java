package com.src.is.view.fragments;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.src.is.R;
import com.src.is.utils.RoundedImageView;
import com.src.is.view.activity.HomeActivity;
import com.src.is.view.adapters.ViewPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.src.is.R.style.AppTheme;

/**
 * Created by insonix on 10/3/17.
 */
public class ProfileKnowledgeSeekerFragment extends BaseFragment {


    @BindView(R.id.image_hover)
    ImageView imageHover;
    @BindView(R.id.image_back_btn)
    ImageView imageBackBtn;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.image_profile)
    RoundedImageView imageProfile;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    Toolbar toolbar;
    private TextView textToolHeader;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ks_profile, container, false);
        ButterKnife.bind(this, view);
        imageHover.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorTint));
        toolbar = (Toolbar) getActivity().findViewById(R.id.appbar);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupViewPager(viewpager);
        addDivider(tabs);
        tabs.setupWithViewPager(viewpager);
    }

    private void addDivider(TabLayout tabs) {
        View root = tabs.getChildAt(0);
        if (root instanceof LinearLayout) {
            ((LinearLayout) root).setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setColor(getResources().getColor(R.color.separator));
            drawable.setSize(2, 1);
            ((LinearLayout) root).setDividerPadding(10);
            ((LinearLayout) root).setDividerDrawable(drawable);
        }
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(new AppointmentsListFragment(), "Older");
        adapter.addFragment(new AppointmentsListFragment(), "Recent");
        viewPager.setAdapter(adapter);
    }

}
