package com.src.is.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.src.is.R;
import com.src.is.view.fragments.HomeFragment;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity implements BackButtonListener {
    @BindView(R.id.appbar)
     Toolbar toolbar;
    @BindView(R.id.slidingpane_layout)
    SlidingPaneLayout slidingpaneLayout;
    private BackButtonListener backButtonListener;

    public static Intent createIntent(Context ctx) {
        Intent intent = new Intent(ctx, HomeActivity.class);
        return intent;
    }

    @Subscribe
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.home);
        backButtonListener = this;
        HomeFragment nextFrag = new HomeFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.content_fragment, nextFrag)
                .addToBackStack(null)
                .commit();
        showNavigAnimation();

    }

    private void showNavigAnimation() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                slidingpaneLayout.openPane();
            }
        }, 1500);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                slidingpaneLayout.closePane();
            }
        }, 3000);
    }

    SlidingPaneLayout.PanelSlideListener panelListener = new SlidingPaneLayout.PanelSlideListener() {

        @Override
        public void onPanelClosed(View arg0) {
            Log.d("close", "close");
        }

        @Override
        public void onPanelOpened(View arg0) {
            Log.d("oeen", "oprn");
          /*  HomeFragment nextFrag = new HomeFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_fragment, nextFrag)
                    .addToBackStack(null)
                    .commit();*/
        }

        @Override
        public void onPanelSlide(View arg0, float arg1) {
            Log.d("slide", "slide");
        }

    };

    @Override
    public void onBackPressed() {
        backButtonListener.onClickBackListener();
        int fragments = getSupportFragmentManager().getBackStackEntryCount();
        if (fragments == 1) {
            finish();
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                getSupportFragmentManager().popBackStack();
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public void onClickBackListener() {

    }

    public Toolbar getToolbar() {
        return toolbar;
    }
}
