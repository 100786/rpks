package com.src.is.view.adapters;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.src.is.BuildConfig;
import com.src.is.R;
import com.src.is.utils.CommonUtils;
import com.src.is.utils.Constants;
import com.src.is.utils.MenuActionItem;
import com.src.is.view.fragments.HomeFragment;
import com.src.is.view.fragments.ProfileKnowledgeSeekerFragment;
import com.src.is.view.fragments.ProfileRetiredFragment;
import com.src.is.view.fragments.SelectGroupFragment;

import static android.R.id.message;


/**
 * Created by Anthony on 16-01-25.
 */
public class MenuListAdapter extends ArrayAdapter<MenuActionItem> {

    int resource;
    Fragment nextFrag;
    AppCompatActivity activity;
    int[] itemsRP = {R.string.home, R.string.profile, R.string.upcoming, R.string.invite, R.string.schedule, R.string.history, R.string.feed, R.string.about};
    int[] itemsKS = {R.string.home, R.string.profile, R.string.upcoming, R.string.invite, R.string.feed, R.string.about};
    int[] titles;

    public MenuListAdapter(int resource, AppCompatActivity activity, MenuActionItem[] items) {
        super(activity, resource, items);
        this.resource = resource;
        this.activity = activity;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if (rowView == null) {
            rowView = activity.getLayoutInflater().inflate(resource, null);
            MenuItemViewHolder viewHolder = new MenuItemViewHolder();
            viewHolder.menuItemImageView = (ImageView) rowView.findViewById(R.id.menu_item_image_view);
            viewHolder.menuItemTextView = (TextView) rowView.findViewById(R.id.menu_item_text_view);
            viewHolder.root = (LinearLayout) rowView.findViewById(R.id.root);
            rowView.setTag(viewHolder);
        }

        MenuItemViewHolder holder = (MenuItemViewHolder) rowView.getTag();
        changeNavigationItems();
        if (position == MenuActionItem.ITEM1.ordinal()) {
            holder.menuItemImageView.setImageDrawable(activity.getDrawable(R.drawable.home));
            holder.menuItemTextView.setText(titles[position]);
            if (BuildConfig.APPFLAVOR == Constants.UserTypeDef.RPUSERTYPE) {
                nextFrag = new HomeFragment();
            } else {
                nextFrag = new SelectGroupFragment();
            }
            openMenuFragment(holder, nextFrag);
        } else if (position == MenuActionItem.ITEM2.ordinal()) {
            holder.menuItemImageView.setImageDrawable(activity.getDrawable(R.drawable.profile));
            holder.menuItemTextView.setText(titles[position]);
            if (BuildConfig.APPFLAVOR == Constants.UserTypeDef.RPUSERTYPE) {
                nextFrag = new ProfileRetiredFragment();
            } else {
                nextFrag = new ProfileKnowledgeSeekerFragment();
            }
            openMenuFragment(holder, nextFrag);
        } else if (position == MenuActionItem.ITEM3.ordinal()) {
            holder.menuItemImageView.setImageDrawable(activity.getDrawable(R.drawable.upcomingappointments));
            holder.menuItemTextView.setText(titles[position]);
            if (BuildConfig.APPFLAVOR == Constants.UserTypeDef.RPUSERTYPE) {
                //TODO
            } else {
                //TODO
            }

        } else if (position == MenuActionItem.ITEM4.ordinal()) {
            holder.menuItemImageView.setImageDrawable(activity.getDrawable(R.drawable.vector_drawable_share));
            holder.menuItemTextView.setText(titles[position]);
            if (BuildConfig.APPFLAVOR == Constants.UserTypeDef.RPUSERTYPE) {
                holder.root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CommonUtils.showDialog(activity);
                    }
                });
            } else {
                //TODO
            }
        } else if (position == MenuActionItem.ITEM5.ordinal()) {
            if (BuildConfig.APPFLAVOR == Constants.UserTypeDef.RPUSERTYPE) {
                holder.menuItemImageView.setImageDrawable(activity.getDrawable(R.drawable.schedule));
                holder.menuItemTextView.setText(titles[position]);
            } else {
                holder.menuItemImageView.setImageDrawable(activity.getDrawable(R.drawable.feed));
                holder.menuItemTextView.setText(titles[position]);
            }

        } else if (position == MenuActionItem.ITEM6.ordinal()) {
            if (BuildConfig.APPFLAVOR == Constants.UserTypeDef.RPUSERTYPE) {
                holder.menuItemImageView.setImageDrawable(activity.getDrawable(R.drawable.history));
                holder.menuItemTextView.setText(titles[position]);
            } else {
                holder.menuItemImageView.setImageDrawable(activity.getDrawable(R.drawable.about_us));
                holder.menuItemTextView.setText(titles[position]);
            }

        } else if (titles.length > 6 & position == MenuActionItem.ITEM7.ordinal()) {
            if (BuildConfig.APPFLAVOR == Constants.UserTypeDef.RPUSERTYPE) {
                holder.menuItemImageView.setImageDrawable(activity.getDrawable(R.drawable.feed));
                holder.menuItemTextView.setText(titles[position]);
            }

        } else if (titles.length > 6 & position == MenuActionItem.ITEM8.ordinal()) {
            holder.menuItemImageView.setImageDrawable(activity.getDrawable(R.drawable.about_us));
            holder.menuItemTextView.setText(titles[position]);

        }/* else if (titles.length > 6 & position == MenuActionItem.ITEM8.ordinal()) {
            holder.menuItemImageView.setImageDrawable(activity.getDrawable(R.drawable.about_us));
            holder.menuItemTextView.setText(titles[position]);
        }*/
        return rowView;
    }

    public void openMenuFragment(MenuItemViewHolder holder, final Fragment nextFrag) {
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_fragment, nextFrag)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    //To change navigation menu as per usertype

    public void changeNavigationItems() {
        if (BuildConfig.APPFLAVOR == Constants.UserTypeDef.KSUSERTYPE) {
            titles = itemsKS;
        } else {
            titles = itemsRP;
        }
    }

    private static class MenuItemViewHolder {
        public ImageView menuItemImageView;
        public TextView menuItemTextView;
        public LinearLayout root;
    }


}
