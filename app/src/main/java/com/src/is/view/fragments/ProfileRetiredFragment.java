package com.src.is.view.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.src.is.BuildConfig;
import com.src.is.R;
import com.src.is.EventBus.Events;
import com.src.is.EventBus.GlobalBus;
import com.src.is.utils.Constants;
import com.src.is.utils.RoundedImageView;
import com.src.is.view.ProfileView;
import com.src.is.view.activity.BackButtonListener;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.ContentValues.TAG;
import static com.src.is.utils.CommonUtils.getCalendar;

/**
 * Created by insonix on 23/12/16.
 */

public class ProfileRetiredFragment extends BaseFragment implements ProfileView, BackButtonListener {

    @BindView(R.id.image_hover)
    ImageView imageHover;
    @BindView(R.id.image_back_btn)
    ImageView imageBackBtn;
    @BindView(R.id.text_follow)
    TextView textFollow;
    @BindView(R.id.linear_follow)
    LinearLayout linearFollow;
    @BindView(R.id.text_last_work_company)
    TextView textLastWorkCompany;
    @BindView(R.id.image_profile)
    RoundedImageView imageProfile;
    @BindView(R.id.text_depict)
    TextView textDepict;
    @BindView(R.id.yt_thumbnail)
    ImageView ytThumbnail;
    @BindView(R.id.image_video_logo)
    ImageView imageVideoLogo;
    @BindView(R.id.card_view)
    CardView cardView;
    @BindView(R.id.btn_book_session)
    Button btnBookSession;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.exp_tv)
    TextView expTv;
    private Events.UserEvent userEvent;

    @Subscribe
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_retired_profile, container, false);
        ButterKnife.bind(this, view);
        imageHover.setColorFilter(ContextCompat.getColor(getActivity(), R.color.colorTint));
        if (BuildConfig.APPFLAVOR == Constants.UserTypeDef.KSUSERTYPE) {
            userEvent = GlobalBus.getBus().getStickyEvent(Events.UserEvent.class);
            Log.i(TAG, "onCreateView: " + userEvent.getmUser().getAddress());
            setProfile(userEvent);
        } else {
            btnBookSession.setVisibility(View.GONE);
            linearFollow.setVisibility(View.INVISIBLE);
        }
        return view;
    }

    @OnClick({R.id.image_back_btn, R.id.btn_book_session})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image_back_btn:
                break;
            case R.id.btn_book_session:
                getCalendar(getActivity());
                break;
        }
    }


    @Override
    public void onClickBackListener() {

    }

    @Override
    public void setProfile(Events.UserEvent userEvent) {
//        Picasso.with(getActivity()).load(userEvent.getmUser().getImage()).error(R.drawable.cast_album_art_placeholder).into(imageProfile);
        name.setText(userEvent.getmUser().getUserName());
        expTv.setText(userEvent.getmUser().getExperience());
        textLastWorkCompany.setText(userEvent.getmUser().getLastWorked());

    }
}
