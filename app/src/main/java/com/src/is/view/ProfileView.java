package com.src.is.view;

import com.src.is.EventBus.Events;

/**
 * Created by insonix on 9/3/17.
 */
public interface ProfileView extends BaseView {

    void setProfile(Events.UserEvent userEvent);

}
