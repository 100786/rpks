package com.src.is.view;

import com.src.is.models.GetSubjectModel;
import com.src.is.models.SignUpModel;
import com.src.is.view.BaseView;

/**
 * Created by insonix on 22/12/16.
 */

public interface SignUpView extends BaseView {

    void showUserNameError();

    void showPasswordError();

    void showConfirmPasswordError();

    void showMatchesPasswordError();

    void showAddressError();

    void showContactError();


    void showContactLengthError();

    void showEmailError();

    void showEmailPatternError();

    void showPasswordLengthError();

    void setSubjectList(GetSubjectModel getSubjectModel);

    void nextView(SignUpModel User);
}
