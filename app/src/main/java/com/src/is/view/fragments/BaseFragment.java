package com.src.is.view.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.src.is.R;
import com.src.is.utils.CommonProgress;
import com.src.is.utils.RPKSApplication;
import com.src.is.view.BaseView;
import com.src.is.view.activity.BaseActivity;

import butterknife.ButterKnife;

/**
 * Created by insonix on 8/3/17.
 */
public class BaseFragment extends Fragment implements BaseView{

    Context ctx;
    public static Dialog dialog;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        injectViews(view);
        ctx= RPKSApplication.getInstance().getApplicationContext();
    }

    private void injectViews(View view) {
        ButterKnife.bind(this, view);
    }

    @Override
    public void onShowProgress() {
        CommonProgress.showProgress(getActivity());    }

    @Override
    public void onHideProgress() {
        CommonProgress.hideProgress();
    }

    @Override
    public void onHideKeyboard() {

    }

    @Override
    public void onErrorMsg(int message) {
        Snackbar.make(getActivity().getWindow().getDecorView(),message,Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onSuccesMsg() {
        Toast.makeText(getActivity(),"Successful",Toast.LENGTH_LONG).show();

    }

    @Override
    public void onServerErrorMsg(String message) {
        Snackbar.make(getActivity().getWindow().getDecorView(),message,Snackbar.LENGTH_LONG).show();
    }


}
