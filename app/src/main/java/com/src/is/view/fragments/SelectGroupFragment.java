package com.src.is.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.src.is.R;
import com.src.is.EventBus.Events;
import com.src.is.EventBus.GlobalBus;
import com.src.is.models.GetSubjectModel;
import com.src.is.models.SubjectInfo;
import com.src.is.presenter.WebResponsePresenter;
import com.src.is.presenter.WebResponseView;

import com.src.is.utils.ItemChildClickListener;
import com.src.is.view.adapters.ExpandableListAdapterGroup;

import org.greenrobot.eventbus.Subscribe;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 20/12/16.
 */

public class SelectGroupFragment extends BaseFragment implements WebResponseView,ItemChildClickListener {


    @BindView(R.id.expand_select_group)
    ExpandableListView expandSelectGroup;
    @BindView(R.id.text_note)
    TextView textNote;
    @BindView(R.id.linear_follow)
    LinearLayout linearFollow;
    private ArrayList<SubjectInfo> listDataHeader;
    private HashMap<String, ArrayList<SubjectInfo>> listDataChild;
    private ExpandableListAdapterGroup listAdapter;
    private WebResponsePresenter webResponsePresenter;

    @Subscribe
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_expandable, container, false);
        ButterKnife.bind(this, view);
        webResponsePresenter = new WebResponsePresenter(this);
        webResponsePresenter.getSubject();
        expandSelectGroup.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    expandSelectGroup.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });
//        userEvent = GlobalBus.getBus().getStickyEvent(Events.UserCreateEvent.class);
//        if (userEvent != null) {
//            Log.i(TAG, "onCreate:testing fragment " + userEvent.getUserId());
//        }
        return view;
    }


    @Override
    public void setSubjectList(GetSubjectModel getSubjectModel) {

        List<SubjectInfo> subjectInfoList = getSubjectModel.getSubjectInfo();
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();

        for (int i = 0; i < subjectInfoList.size(); i++) {
            listDataHeader.add(subjectInfoList.get(i));

            ArrayList<SubjectInfo> subSubjectList = new ArrayList<>();
            for (int j = 0; j < subjectInfoList.get(i).getSubSubject().size(); j++) {
                subSubjectList.add(subjectInfoList.get(i).getSubSubject().get(j));
            }
            listDataChild.put(subjectInfoList.get(i).getSubjectName(), subSubjectList);
        }

        listAdapter = new ExpandableListAdapterGroup(getActivity(), listDataHeader, listDataChild,this);
        expandSelectGroup.setAdapter(listAdapter);
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onChildClick(int position, SubjectInfo subject) {
        Log.i(TAG, "onChildClick: "+subject.getSubjectName());
        //posting data to event bus
        GlobalBus.getBus().postSticky(new Events.SubjectEvent(subject.getSubjectId()));
        RpUsersListingFragment nextFrag = new RpUsersListingFragment();
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_fragment, nextFrag)
                .addToBackStack(null)
                .commit();
    }
}
