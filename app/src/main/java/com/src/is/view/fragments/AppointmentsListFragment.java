package com.src.is.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.src.is.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by insonix on 10/3/17.
 */
public class AppointmentsListFragment extends BaseFragment {


    @BindView(R.id.notavlbl)
    TextView notavlbl;
    @BindView(R.id.appointmentsList)
    RecyclerView appointmentsList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_appointment_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    private void setupList() {
      /*  AppointmentAdapter mAdapter = new AppointmentAdapter(usersList, this);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerChildList.setLayoutManager(mLayoutManager);
        recyclerChildList.setItemAnimator(new DefaultItemAnimator());
        recyclerChildList.setAdapter(groupChildMembersAdapter);*/
    }
}
