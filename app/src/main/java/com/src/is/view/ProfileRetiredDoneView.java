package com.src.is.view;

/**
 * Created by insonix on 21/12/16.
 */

public interface ProfileRetiredDoneView {

     void onDone(String hourlyRate, String experience, String lastWorkExp, String designation);

    void getSubjects();
}
