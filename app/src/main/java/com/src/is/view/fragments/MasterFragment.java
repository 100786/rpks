package com.src.is.view.fragments;

import android.app.ListFragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.src.is.R;
import com.src.is.utils.MenuActionItem;
import com.src.is.view.adapters.MenuListAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MasterFragment extends ListFragment {


    @BindView(android.R.id.list)
    ListView list;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_master, container);
        ButterKnife.bind(this, view);
        setListAdapter(new MenuListAdapter(R.layout.row_menu_action_item, (AppCompatActivity) getActivity(), MenuActionItem.values()));
//        setHeader(inflater);
        return view;
    }
    public void setHeader(LayoutInflater inflater) {
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.row_menu_header, list, false);
        list.addHeaderView(header, null, false);
    }

}
