package com.src.is.view.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

import com.src.is.R;
import com.src.is.EventBus.GlobalBus;
import com.src.is.view.BaseView;

import org.greenrobot.eventbus.Subscribe;

/**
 * Created by insonix on 6/3/17.
 */

public abstract class BaseActivity extends AppCompatActivity implements BaseView{


    private static final String TAG = BaseActivity.class.getName();
    private String userId;
    public static Dialog dialog;


    @Subscribe
    @Override
    protected void onStart() {
        super.onStart();
        GlobalBus.getBus().register(this);
        dialog = new Dialog(this, android.R.style.Theme_Translucent);
        View views = LayoutInflater.from(this).inflate(R.layout.dot_dialog, null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
//        dialog.getWindow().setBackgroundDrawableResource(R.color.feed_item_bg);
        dialog.setContentView(views);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
    }

    @Override
    public void onShowProgress() {
        dialog.show();
    }

    @Override
    public void onHideProgress() {
        dialog.dismiss();
    }

    @Override
    public void onHideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onErrorMsg(int message) {
        Snackbar.make(getWindow().getDecorView(),message,Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onSuccesMsg() {

    }


    @Override
    public void onServerErrorMsg(String message) {
        Snackbar.make(getWindow().getDecorView(),message,Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onStop() {
        GlobalBus.getBus().unregister(this);
        super.onStop();
    }

}
