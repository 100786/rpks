package com.src.is.view;

import com.src.is.models.RetiredUserList;

/**
 * Created by insonix on 8/3/17.
 */
public interface RpUsersListView extends BaseView{

    void setRpUsersList(RetiredUserList list);
}
