package com.src.is.view;

import com.src.is.models.Login;

/**
 * Created by insonix on 6/3/17.
 */
public interface LoginView extends BaseView{

    void onNextView(Login loginResponse);

    boolean validateInput(String userName, String pwd);
}
