package com.src.is.view;

import com.src.is.models.GetSubjectModel;

/**
 * Created by insonix on 22/12/16.
 */

public interface SignUpSubmitView  {

    public void onSubmit(String userName, String password, String confirmPassword, String email, String address, String contact,int subjectId,int subSubjectId,int userType,int apiType);

    public void onKsSubmit(String userName, String password, String confirmPassword, String email, String address, String contact,int userType,int subjectId,int subSubjectId,int apiType);
}
