package com.src.is.view.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.squareup.picasso.Picasso;
import com.src.is.R;
import com.src.is.models.SubjectInfo;
import com.src.is.utils.ItemChildClickListener;

public class ExpandableListAdapterGroup extends BaseExpandableListAdapter {

    private Activity _context;
    private List<SubjectInfo> _listDataHeader;
    private HashMap<String,ArrayList<SubjectInfo>> _listDataChild;
    ItemChildClickListener mListener;

    public ExpandableListAdapterGroup(Activity context, List<SubjectInfo> listDataHeader,
                                      HashMap<String, ArrayList<SubjectInfo>> listChildData,ItemChildClickListener listener) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.mListener=listener;
    }

    @Override
    public SubjectInfo getChild(int groupPosition, int childPosititon) {
        return _listDataChild.get(_listDataHeader.get(groupPosition).getSubjectName())
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final SubjectInfo childObj = (SubjectInfo) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.adapter_child_item, null);
        }

        final TextView txtListChild = (TextView) convertView
                .findViewById(R.id.text_child_item);
        txtListChild.setText(childObj.getSubjectName());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               mListener.onChildClick(childPosition,childObj);
            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return _listDataChild.get(_listDataHeader.get(groupPosition).getSubjectName())
                .size();
    }

    @Override
    public SubjectInfo getGroup(int groupPosition) {
        return _listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return _listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = getGroup(groupPosition).getSubjectName();
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.adapter_expandable_group, null);
        }
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.text_group_name);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
        ImageView groupImage=(ImageView)convertView.findViewById(R.id.image_group_type);
        Animation animation = AnimationUtils.loadAnimation(_context,
                R.anim.myanimation);
        groupImage.startAnimation(animation);
        Picasso.with(_context).load(_listDataHeader.get(groupPosition).getImage()).resize(80,80).into(groupImage);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}