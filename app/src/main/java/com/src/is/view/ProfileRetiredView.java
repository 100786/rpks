package com.src.is.view;


import com.src.is.models.GetSubjectModel;
import com.src.is.view.BaseView;

/**
 * Created by insonix on 21/12/16.
 */

public interface ProfileRetiredView extends BaseView{
    void showHourlyError();

    void showExperienceError();

    void showLastWorkExpError();

    void showDesignationError();

    void onUpdateCompletion();


    void setSubjectList(GetSubjectModel getSubjectModel);
}
