package com.src.is.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.src.is.BuildConfig;
import com.src.is.R;
import com.src.is.utils.Constants;
import com.src.is.utils.MyPreferences;

import static android.content.ContentValues.TAG;

public class SplashActivity extends AppCompatActivity {
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Log.i(TAG, "onCreate: " + MyPreferences.getInstance().getString(MyPreferences.Key.USERID));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (MyPreferences.getInstance().getString(MyPreferences.Key.USERID) == null) {
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                } else {
                    if (BuildConfig.APPFLAVOR == Constants.UserTypeDef.KSUSERTYPE) {
                        startActivity(SelectGroupActivity.createIntent(SplashActivity.this));
                    } else {
                        if(MyPreferences.getInstance().getBoolean(MyPreferences.Key.FIRST_TIME)){
                            startActivity(EditProfileRetiredActivity.createIntent(SplashActivity.this));
                        }else{
                            startActivity(HomeActivity.createIntent(SplashActivity.this));
                        }
                    }
                }
                finish();
            }
        }, 2000);

    }

}
