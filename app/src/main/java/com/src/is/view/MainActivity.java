package com.src.is.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.src.is.R;
import com.src.is.utils.MyPreferences;
import com.src.is.view.activity.SplashActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity{
    @BindView(R.id.ks)
    Button ks;
    @BindView(R.id.rp)
    Button rp;


    MyPreferences myPreferences;

//    @BindView(R.id.edit_query)
//    EditText editQuery;
//    @BindView(R.id.activity_main)
//    LinearLayout activityMain;

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activtiy_main1);
        ButterKnife.bind(this);
    }
    @OnClick({R.id.ks, R.id.rp})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ks:
                myPreferences.put(MyPreferences.Key.USER_TYPE,"ks");
                Intent intent = new Intent(MainActivity.this, SplashActivity.class);
                startActivity(intent);

                break;
            case R.id.rp:
                myPreferences.put(MyPreferences.Key.USER_TYPE,"rp");
                Intent intent1 = new Intent(MainActivity.this, SplashActivity.class);
                startActivity(intent1);
                break;
        }
    }
}
