package com.src.is.view.activity;

/**
 * Created by insonix on 28/12/16.
 */

public interface BackButtonListener {
    void onClickBackListener();
}
