package com.src.is.view.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.src.is.R;
import com.src.is.presenter.SignUpRetiredPresenter;

/**
 * Created by insonix on 15/12/16.
 */

public class SignUpRetiredActivity extends AppCompatActivity {

    @BindView(R.id.input_username)
    EditText inputUsername;
    @BindView(R.id.input_layout_username)
    TextInputLayout inputLayoutUsername;
    @BindView(R.id.input_email)
    EditText inputEmail;
    @BindView(R.id.input_layout_email)
    TextInputLayout inputLayoutEmail;
    @BindView(R.id.input_contact)
    EditText inputContact;
    @BindView(R.id.input_layout_contact)
    TextInputLayout inputLayoutContact;
    @BindView(R.id.area_expertise)
    TextView areaExpertise;
    @BindView(R.id.spinner_category)
    Spinner spinnerCategory;
    @BindView(R.id.spinner_subcategory)
    Spinner spinnerSubcategory;
    @BindView(R.id.input_address)
    EditText inputAddress;
    @BindView(R.id.input_layout_address)
    TextInputLayout inputLayoutAddress;
    @BindView(R.id.btn_sign_up)
    Button btnSignUp;
    SignUpRetiredPresenter signUpRetiredPresenter;

    String mCategory[] = {"Select Category", "Engineer", "Doctor", "Officer"};
    String mSubCategory[] = {"Select Sub-Category", "Engineer", "Doctor", "Officer"};
    ArrayAdapter categoryAdapter, subCategoryAdapter;
    @BindView(R.id.input_password)
    EditText inputPassword;
    @BindView(R.id.input_layout_password)
    TextInputLayout inputLayoutPassword;
    @BindView(R.id.input_confirm_password)
    EditText inputConfirmPassword;
    @BindView(R.id.input_layout_confirm_password)
    TextInputLayout inputLayoutConfirmPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

//        signUpRetiredPresenter = new SignUpRetiredPresenter(this,this);
        areaExpertise.setText(getString(R.string.area_expertise));
        categoryAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, mCategory);
        subCategoryAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, mSubCategory);
        spinnerCategory.setAdapter(categoryAdapter);
        spinnerSubcategory.setAdapter(subCategoryAdapter);
        spinnerCategory.setSelection(0);
        spinnerSubcategory.setSelection(0);

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != 0)
                    spinnerSubcategory.setVisibility(View.VISIBLE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }


    @OnClick(R.id.area_expertise)
    public void onClick() {
        spinnerCategory.setVisibility(View.VISIBLE);
    }

}
