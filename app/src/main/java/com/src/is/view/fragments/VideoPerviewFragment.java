package com.src.is.view.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;


import com.src.is.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by insonix on 21/12/16.
 */

public class VideoPerviewFragment extends Fragment {
    @BindView(R.id.videoPreview)
    VideoView videoPreview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_video_perview, container,false);
        ButterKnife.bind(this, view);
        Log.d("video",getArguments().getString("VideoPath"));
        videoPreview.setVisibility(View.VISIBLE);
        videoPreview.setVideoPath(getArguments().getString("VideoPath"));
        videoPreview.start();
        return view;
    }
}
