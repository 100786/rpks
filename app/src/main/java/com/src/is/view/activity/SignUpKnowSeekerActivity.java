package com.src.is.view.activity;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.src.is.R;
import com.src.is.EventBus.Events;
import com.src.is.EventBus.GlobalBus;
import com.src.is.models.GetSubjectModel;
import com.src.is.models.SignUpModel;
import com.src.is.models.SubjectInfo;
import com.src.is.presenter.SignUpRetiredPresenter;
import com.src.is.utils.Constants;
import com.src.is.utils.MyPreferences;
import com.src.is.view.SignUpView;
import com.src.is.view.adapters.NothingSelectedSpinnerAdapter;
import com.src.is.view.adapters.SubjectAdapter;

import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SignUpKnowSeekerActivity extends BaseActivity implements SignUpView {

    private static final String TAG = SignUpKnowSeekerActivity.class.getName();
    @BindView(R.id.input_username)
    EditText inputUsername;
    @BindView(R.id.input_layout_username)
    TextInputLayout inputLayoutUsername;
    @BindView(R.id.input_email)
    EditText inputEmail;
    @BindView(R.id.input_layout_email)
    TextInputLayout inputLayoutEmail;
    @BindView(R.id.input_contact)
    EditText inputContact;
    @BindView(R.id.input_layout_contact)
    TextInputLayout inputLayoutContact;

    @BindView(R.id.spinner_category)
    Spinner spinnerCategory;
    @BindView(R.id.input_password)
    EditText inputPassword;
    @BindView(R.id.input_layout_password)
    TextInputLayout inputLayoutPassword;
    @BindView(R.id.input_confirm_password)
    EditText inputConfirmPassword;
    @BindView(R.id.input_layout_confirm_password)
    TextInputLayout inputLayoutConfirmPassword;
    @BindView(R.id.btn_sign_up)
    Button btnSignUp;
    @BindView(R.id.spinner_areaExpertise)
    Spinner spinnerAreaExpertise;
    @BindView(R.id.linear_area)
    CardView linearArea;
    @BindView(R.id.input_address)
    EditText inputAddress;
    @BindView(R.id.input_layout_address)
    TextInputLayout inputLayoutAddress;
    SubjectAdapter subjectAdapter;
    @BindView(R.id.category_lay)
    CardView categoryLay;
    SignUpRetiredPresenter signUpRetiredPresenter;
    private String mName, mAddress, mContact, mEmail, mPassword, mConformPassword;
    int mSubjectId = 1, mSubSubjectId = 1;
    @Constants.UserTypeDef
    int userType;
    @Constants.ApiTypeDef
    int api_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_layout);
        ButterKnife.bind(this);
        signUpRetiredPresenter = new SignUpRetiredPresenter(this, this);
        signUpRetiredPresenter.getSubject();
    }

    @OnClick({R.id.linear_area, R.id.btn_sign_up})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sign_up:

                if (MyPreferences.getInstance().getString(MyPreferences.Key.USER_TYPE).equals("rp")) {
                    userType = Constants.UserTypeDef.RPUSERTYPE;
                    api_type = Constants.ApiTypeDef.APITYPE_UPDATE;
                    mName = inputUsername.getText().toString().trim();
                    mAddress = inputAddress.getText().toString().trim();
                    mContact = inputContact.getText().toString().trim();
                    mEmail = inputEmail.getText().toString().trim();
                    mConformPassword = inputConfirmPassword.getText().toString().trim();
                    mPassword = inputPassword.getText().toString().trim();
                    signUpRetiredPresenter.onSubmit(mName, mPassword, mConformPassword, mEmail, mAddress, mContact, mSubjectId, mSubSubjectId, userType, api_type);

                } else {
                    userType = Constants.UserTypeDef.KSUSERTYPE;
                    api_type = Constants.ApiTypeDef.APITYPE_REGISTER;
                    mName = inputUsername.getText().toString().trim();
                    mContact = inputContact.getText().toString().trim();
                    mEmail = inputEmail.getText().toString().trim();
                    mConformPassword = inputConfirmPassword.getText().toString().trim();
                    mPassword = inputPassword.getText().toString().trim();
                    signUpRetiredPresenter.onKsSubmit(mName, mPassword, mConformPassword, mEmail, "", mContact, mSubjectId, mSubSubjectId, userType, api_type);
                }
                break;
        }
    }

    @Override
    public void showUserNameError() {
        Snackbar.make(inputUsername, R.string.username_empty_error_msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showPasswordError() {
        Snackbar.make(inputPassword, R.string.password_empty_error_msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showConfirmPasswordError() {
        Snackbar.make(inputConfirmPassword, R.string.confirm_password_empty_error_msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showMatchesPasswordError() {
        Snackbar.make(inputConfirmPassword, R.string.matches_password_empty_error_msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showAddressError() {
        Snackbar.make(inputAddress, R.string.address_empty_error_msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showContactError() {
        Snackbar.make(inputContact, R.string.contact_empty_error_msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showContactLengthError() {
        Snackbar.make(inputContact, R.string.contact_length_error_msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showEmailError() {
        Snackbar.make(inputEmail, R.string.email_empty_error_msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showEmailPatternError() {
        Snackbar.make(inputEmail, R.string.email_pattern_error_msg, Snackbar.LENGTH_LONG).show();
    }


    @Override
    public void showPasswordLengthError() {
        Snackbar.make(inputPassword, R.string.password_length_error_msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void setSubjectList(GetSubjectModel getSubjectModel) {
        final List<SubjectInfo> subjectInfoList = getSubjectModel.getSubjectInfo();
        subjectAdapter = new SubjectAdapter(SignUpKnowSeekerActivity.this, android.R.layout.simple_list_item_1, subjectInfoList);
        subjectAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAreaExpertise.setAdapter(new NothingSelectedSpinnerAdapter(subjectAdapter,
                R.layout.contact_spinner_row_nothing_selected,
                this));
        spinnerAreaExpertise.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.i(TAG, "onItemSelected: " + position);
                if (position > 0) {
                    categoryLay.setVisibility(View.VISIBLE);
                    setSubSubjectList(position, subjectInfoList);
                    mSubjectId=Integer.parseInt(subjectInfoList.get(position-1).getSubjectId());        //nothing selected adapter added "hint" as )th item
                    Log.i(TAG, "onItemSelected: "+mSubjectId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                categoryLay.setVisibility(View.GONE);

            }
        });
    }

    private void setSubSubjectList(int position, List<SubjectInfo> parentData) {
        Log.i(TAG, "setSubSubjectList: "+position);
        if (position > 0 && parentData.get(position-1).getSubSubject()!=null) {
            final List<SubjectInfo> subSubjectList=parentData.get(position-1).getSubSubject();
            spinnerCategory.setAdapter(new SubjectAdapter(SignUpKnowSeekerActivity.this, android.R.layout.simple_list_item_1, subSubjectList));
            spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mSubSubjectId= Integer.parseInt(subSubjectList.get(position).getSubjectId());
                    Log.i(TAG, "onItemSelected: "+mSubSubjectId);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(spinnerAreaExpertise.getSelectedItemPosition()==0)
            signUpRetiredPresenter.getSubject();
    }

    @Override
    public void nextView(SignUpModel user) {
        finish();
        GlobalBus.getBus().postSticky(new Events.UserCreateEvent(user.getUserId()));
        MyPreferences.getInstance().put(MyPreferences.Key.USERID,user.getUserId());
        startActivity(SelectGroupActivity.createIntent(SignUpKnowSeekerActivity.this));
    }


    @Subscribe
    public void onUserCreateEvent(Events.UserCreateEvent userEvent){
//        Toast.makeText(SignUpKnowSeekerActivity.this,"event Captured"+userEvent.getUserId(),Toast.LENGTH_LONG).show();
    }

    @Override
    public void onShowProgress() {
    }

    @Override
    public void onHideProgress() {
    }

    @Override
    public void onErrorMsg(int message) {
        Snackbar.make(btnSignUp, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onSuccesMsg() {
        Snackbar.make(btnSignUp, R.string.signup_success, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onServerErrorMsg(String message) {
        Snackbar.make(btnSignUp, message, Snackbar.LENGTH_LONG).show();
    }

}
