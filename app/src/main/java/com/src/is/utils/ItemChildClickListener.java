package com.src.is.utils;

import com.src.is.models.SubjectInfo;

/**
 * Created by insonix on 7/3/17.
 */
public interface ItemChildClickListener {

    void onChildClick(int position, SubjectInfo subject);

}
