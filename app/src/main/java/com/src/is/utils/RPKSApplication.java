package com.src.is.utils;

import android.app.Application;
import android.content.Context;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.google.firebase.auth.FirebaseAuth;
import com.src.is.api.ApiClient;
import com.src.is.api.ApiInterface;
import com.src.is.connectivity.ConnectionReceiver;
import com.src.is.connectivity.ConnectivityReceiveListener;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.TimeUnit;

/**
 * Created by insonix on 26/12/16.
 */

public class RPKSApplication extends MultiDexApplication {
    private ConnectionReceiver receiver;
    public static RPKSApplication rpksApplication;


    @Override
    public void onCreate() {
        super.onCreate();
        rpksApplication = this;
        receiver = new ConnectionReceiver();
        MultiDex.install(this);
        SystemClock.sleep(TimeUnit.SECONDS.toMillis(3));
    }

    public static synchronized RPKSApplication getInstance() {
        return rpksApplication;
    }

    public void setConnectivityListener(ConnectivityReceiveListener listener) {
        ConnectionReceiver.connectivityReceiverListener = listener;
    }

    public void checkConnection(@NonNull Context context) {
        receiver.checkInternetConnection(context);
    }

    public ApiInterface initApi(){
        ApiClient apiClient=new ApiClient(getApplicationContext());
        ApiInterface apiService = apiClient.getClient().create(ApiInterface.class);
        return apiService;
    }

}
