package com.src.is.utils;

/**
 * Created by insonix on 7/3/17.
 */
public interface ItemClickListener {

    void onParentClick(int position);

}
