package com.src.is.utils;

import android.support.annotation.IntDef;
import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.src.is.utils.Constants.ApiTypeDef.APITYPE_REGISTER;
import static com.src.is.utils.Constants.ApiTypeDef.APITYPE_UPDATE;
import static com.src.is.utils.Constants.UserTypeDef.KSUSERTYPE;
import static com.src.is.utils.Constants.UserTypeDef.RPUSERTYPE;


/**
 * Created by insonix on 27/12/16.
 */

public interface Constants {
    String OS = "android";
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String SUBJECTID = "subSubjectID";
    int ACTION_TAKE_VIDEO = 101;
    int ACTION_TAKE_PHOTO = 102;

    //ENUM for usertype
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({RPUSERTYPE, KSUSERTYPE})
    @interface UserTypeDef {
        int RPUSERTYPE = 1;
        int KSUSERTYPE = 2;
    }

    void setTypeDef(@UserTypeDef int type);

    @Constants.UserTypeDef
    int getTypeDef();

    //ENUM for apitype
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({APITYPE_REGISTER, APITYPE_UPDATE})
    @interface ApiTypeDef {
        int APITYPE_REGISTER = 1;
        int APITYPE_UPDATE = 2;
    }

    void setApiTypeDef(@ApiTypeDef int type);

    @Constants.ApiTypeDef
    int getApiTypeDef();

}
