package com.src.is.EventBus;

import com.src.is.models.UserInfo;

/**
 * Created by insonix on 7/3/17.
 */
public class Events {

    //Event used to send subjectId to fragment
    public static class SubjectEvent {

        public String getId() {
            return id;
        }

        public String id = "";

        public SubjectEvent(String id) {
            this.id = id;
        }
    }

    //Event used to send userId to All
    public static class UserCreateEvent {

        public String getUserId() {
            return userId;
        }

        String userId;

        public UserCreateEvent(String userId) {
            this.userId = userId;
        }
    }

    //Event used to send userId to All
    public static class UserEvent {

        public UserInfo getmUser() {
            return mUser;
        }

        UserInfo mUser;

        public UserEvent(UserInfo user) {
            this.mUser = user;
        }
    }
}

