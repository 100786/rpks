package com.src.is.EventBus;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by insonix on 8/3/17.
 */

public class GlobalBus {
        private static EventBus sBus;
        public static EventBus getBus() {
            if (sBus == null)
                sBus = EventBus.getDefault();
            return sBus;
        }
}
